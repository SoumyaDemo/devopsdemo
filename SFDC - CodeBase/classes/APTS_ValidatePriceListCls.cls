/*
Class Name        # APTS_ValidatePriceListCls 
Description       # This Class helps to validate Quote Pricelist and Account Segment 
                    while cloning the quote through salesforce FLOW based on input
Created By        # Soumya Behera
Created Date      # JUL-11-2019
Modification Log: #SOUMYA BEHERA MERGE check###
---------------------------------------------------------------------------------------------------------------
Developer                   Date                   Description
--------------------------------------------------------------------------------------------------------------- 
Soumya Behera               JUL-11-2019              Initial Version 

*/
public with sharing class APTS_ValidatePriceListCls {
    public ID QuoteId;
    public String outputStr{get; set;}
    public Flow.Interview.APTS_Price_List_Verification myFlow {get; set;}
    
    public APTS_ValidatePriceListCls(ApexPages.StandardController controller) {
        QuoteId= ApexPages.currentPage().getParameters().get('id');
    }
    /************************************************************************************
    Method Name    : validatePriceList
    Description    : This method used to validate the Quote Pricelist and Account's segment value
    Return Type    : Page Reference
    Author         : Soumya Behera
    Parameter      : Empty
    *************************************************************************************/
    public PageReference  validatePriceList() {   
        Map<String, Object> inputMap = new Map<String, Object>();
        inputMap.put(System.Label.APTS_VariableQuoteID, QuoteId);
        myFlow = new Flow.Interview.APTS_Price_List_Verification(inputMap);
        myFlow.start();     
        outputStr = (string) myFlow.getVariableValue(System.Label.APTS_varOutputMessage);
        if(String.isNotEmpty(outputStr)) {   
            String FlowNamewithErrorMsg = System.label.APTS_QuoteErrorFlowName+outputStr;
            PageReference reference = new PageReference(FlowNamewithErrorMsg);   
            reference.setRedirect(true);
            reference.getParameters().put(System.Label.APTS_retURL,QuoteId );  
            return reference;           
        }else{
            PageReference reference = new PageReference(System.Label.APTS_RegisterQuoteCloneFlowName);
            reference.setRedirect(true);
            //reference.getParameters().put('hardcode value',QuoteId ); // Commented as it was hardcoded 
			reference.getParameters().put(System.Label.APTS_InputVarName,QuoteId );  			
            reference.getParameters().put(System.Label.APTS_retURL, QuoteId+'/e?clone=1');     
            return reference;
        }       
        
    }
}