/***************************************************************************************
Class Name      : CPQ_populateFrenchNameOrSOC
Description     : Class to Populate the French name(On Line Item) or SOC(On COP Line Item) on basis on Product Attributes values. (US - 0632, US-0604)
                  page.
Created By      : Aakankhsa Patel
Created Date    : 7th Apr 16 
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------  

Aakanksha Patel           7th Apr 16          Populate the French name(On Line Item) or SOC(On COP Line Item) on basis on Product Attributes values. (US - 0632, US-0604) page.
*****************************************************************************************/
public with sharing class CPQ_populateFrenchNameOrSOC{
    public static Integer countOfSOCTransCriteria = 0;
    public static Integer countOfFrenchTransCriteria = 0;
    
    /*********************************************************************************
    Method Name    : populateFrenchNameOrSOC
    Description    : 1) Populating the French name or SOC on basis on Product Attributes values. (US - 0632, US-0604)
    Return Type    : Void
    Parameter      : List<Apttus_Config2__LineItem__c>
    Developer      : Aakanksha Patel
    *********************************************************************************/

    public static void  populateFrenchNameOrSOC(LIST<Apttus_Config2__LineItem__c> lstFromTrigger){    
        Set<Apttus_Config2__LineItem__c> setLIUpdate = new Set<Apttus_Config2__LineItem__c>();
        Map<String,String> mapTCFieldNameFieldValue = new Map<String,String>();
        List<Apttus_Config2__LineItem__c> lstLineItem = new List<Apttus_Config2__LineItem__c>();
        List<Translation_Criteria__c> lstTransCri = new List<Translation_Criteria__c>();
        List<Translation__c> lstTranslation = new List<Translation__c>();
        Map<id,List<Translation_Criteria__c>> mapProdTC = new  Map<id,List<Translation_Criteria__c>>();  
        List<Translation_Criteria__c> lstTC = new List<Translation_Criteria__c>();
        Set<ID> setProductId = new Set<ID>();
        Set<ID> setLineItemId = new Set<ID>();// Added by SOumya Behera
        Set<ID> setLineItemId2 = new Set<ID>();
		
        Map<Id,Map<Id,Map<String,List<Translation_Criteria__c>>>> ProductIdWithTranslationMap = new Map<Id,Map<Id,Map<String,List<Translation_Criteria__c>>>>();
        Id NameRecordTypeId = Schema.SObjectType.Translation__c.getRecordTypeInfosByName().get('Name Translation').getRecordTypeId();
        system.debug('*****NameRecordTypeId***'+NameRecordTypeId);
        Id SOCRecordTypeId = Schema.SObjectType.Translation__c.getRecordTypeInfosByName().get('SOC Translation').getRecordTypeId();
        system.debug('*****SOCRecordTypeId ***'+NameRecordTypeId);
        //Looping through Line Items to get the related ptoducts(for dynamic query for Translation) and set of Line item Ids(for query on Line Item)
        
        Set<String> picklist = new Set<String>();
        Schema.DescribeFieldResult fieldResult = Translation_Criteria__c.Field_Name__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

        for( Schema.PicklistEntry f : ple){
            picklist.add(f.getValue());
        }
         for(Apttus_Config2__LineItem__c oLIrec: lstFromTrigger)
        {
            //If the Line Type is option considering the Product in Option field else consider the product field
            if(oLIrec.Apttus_Config2__OptionId__c !=null && oLIrec.Apttus_Config2__LineType__c == 'Option'){
                setProductId.add(oLIrec.Apttus_Config2__OptionId__c);
            }
            else if(oLIrec.Apttus_Config2__ProductId__c !=null && oLIrec.Apttus_Config2__LineType__c == 'Product/Service'){ 
                setProductId.add(oLIrec.Apttus_Config2__ProductId__c);
            } 
            setLineItemId.add(oLIrec.Id);
        }
        Schema.DescribeSObjectResult r = Apttus_Config2__LineItem__c.sObjectType.getDescribe();
        String sAPIFieldNames = '';
        for(string sApiName : r.fields.getMap().keySet()){
            if(sAPIFieldNames !='' && sApiName !='Id'){
                 sAPIFieldNames = sAPIFieldNames + ',' +sApiName;
            }else if(sApiName !='Id'){
                sAPIFieldNames = sApiName;
            }
        }
        //Creating a dynamic query on Line Item object to be passed on to the class"CPQ_populateFrenchNameOrSOC" for populating SOC
        String sQueryLineItem = 'SELECT id';
        sQueryLineItem += String.isBlank(sAPIFieldNames) ? '' : ',' + sAPIFieldNames;
        sQueryLineItem += ' FROM Apttus_Config2__LineItem__c where id = :setLineItemId';
        lstLineItem = Database.query(sQueryLineItem);
     
        if(!setProductId.IsEmpty()){
            lstTransCri = [Select Id,Field_Name__c,Field_Value__c,Translation__c,Translation__r.Count_of_Translation_Criteria__c,Translation__r.French_Name__c,Translation__r.RecordType.Name,Translation__r.Original_SOC__c,Translation__r.Product_Code__c,Translation__r.Rogers_Branded_Name__c,Translation__r.Product_Name__c from Translation_Criteria__c where Translation__r.Product_Name__c IN :  setProductId order by Translation__r.Count_of_Translation_Criteria__c];
        }
        
        for(Translation_Criteria__c tc : lstTransCri){
            if(!ProductIdWithTranslationMap.containsKey(tc.Translation__r.Product_Name__c)){
                Map<Id,Map<String,List<Translation_Criteria__c>>> newMap = new Map<Id,Map<String,List<Translation_Criteria__c>>>();
                Map<String,List<Translation_Criteria__c>> newInnerMap = new Map<String,List<Translation_Criteria__c>>();
                newInnerMap.put(tc.Translation__r.RecordType.Name,new List<Translation_Criteria__c>{tc} );
                newMap.put(tc.Translation__c,newInnerMap);
                ProductIdWithTranslationMap.put(tc.Translation__r.Product_Name__c,newMap);
            }else{
                if(!ProductIdWithTranslationMap.get(tc.Translation__r.Product_Name__c).containsKey(tc.Translation__c)){
                    Map<String,List<Translation_Criteria__c>> newInnerMap = new Map<String,List<Translation_Criteria__c>>();
                    newInnerMap.put(tc.Translation__r.RecordType.Name,new List<Translation_Criteria__c>{tc} );
                    ProductIdWithTranslationMap.get(tc.Translation__r.Product_Name__c).put(tc.Translation__c,newInnerMap);
                }else{
                    if(!ProductIdWithTranslationMap.get(tc.Translation__r.Product_Name__c).get(tc.Translation__c).containsKey(tc.Translation__r.RecordType.Name)){
                        ProductIdWithTranslationMap.get(tc.Translation__r.Product_Name__c).get(tc.Translation__c).put(tc.Translation__r.RecordType.Name,new List<Translation_Criteria__c>{tc} );
                    }else{
                        ProductIdWithTranslationMap.get(tc.Translation__r.Product_Name__c).get(tc.Translation__c).get(tc.Translation__r.RecordType.Name).add(tc) ;
                    }
                
                }
            }
        }
        
        system.debug('******ProductIdWithTranslationMap******'+ProductIdWithTranslationMap);
         //oLI.SOC_Code__c = lstTCfromProd[i].Translation__r.Original_SOC__c;
         //oLI.French_Name__c = lstTCfromProd[i].Translation__r.French_Name__c;
         //oLI.Rogers_Branded_Name__c = lstTCfromProd[i].Translation__r.Rogers_Branded_Name__c;
         //oLI.French_Name__c = oLI.Product_Name_on_Document__c;
         
          //Getting the fields from the Line Item object to create a dynamic query 
        Map<String, Schema.SObjectField> mapFieldAPIandValueLineItem = Schema.SObjectType.Apttus_Config2__LineItem__c.fields.getMap();
        for(Apttus_Config2__LineItem__c oLIrec: lstLineItem){
            Id productId;
             if(oLIrec.Apttus_Config2__OptionId__c !=null && oLIrec.Apttus_Config2__LineType__c == 'Option'){
                productId = oLIrec.Apttus_Config2__OptionId__c;
             }else if(oLIrec.Apttus_Config2__ProductId__c !=null && oLIrec.Apttus_Config2__LineType__c == 'Product/Service'){
                productId = oLIrec.Apttus_Config2__ProductId__c;
             }
             system.debug('*****productId*****'+productId);
             if(String.IsNOTBLANK(productId )){
                  system.debug('*****inside productId*****'+productId);
                //Key =>> oLIrec.Apttus_Config2__OptionId__c
                if(ProductIdWithTranslationMap.containsKey(productId)){
                    for(Id idz : ProductIdWithTranslationMap.get(productId).keyset() ){
                        system.debug('****idz**********'+idz);
                        countOfSOCTransCriteria = 0;
                        countOfFrenchTransCriteria = 0;
                        system.debug('****ProductIdWithTranslationMap.get(productId).get(idz)**********'+ProductIdWithTranslationMap.get(productId).get(idz).containsKey('Name Translation'));
                        if(ProductIdWithTranslationMap.get(productId).get(idz).containsKey('Name Translation')){
                            for(Translation_Criteria__c tc: ProductIdWithTranslationMap.get(productId).get(idz).get('Name Translation')){
                                
                                String sValue;
                                Schema.SObjectField field;
                                if(mapFieldAPIandValueLineItem.get(tc.Field_Name__c)!=null){
                                    field = mapFieldAPIandValueLineItem.get(tc.Field_Name__c);
                                    sValue = String.valueOf(oLIrec.get(field));
                                    system.debug('**field*****sValue****'+sValue+'*******'+field);
                                }//countOfFrenchTransCriteria
                                if(field != null){
                                     if(tc.Field_Value__c == sValue){
                                        countOfFrenchTransCriteria++;
                                    }
                                }
                            }
                            system.debug('***countOfFrenchTransCriteria********'+countOfFrenchTransCriteria);
                            if(countOfFrenchTransCriteria == ProductIdWithTranslationMap.get(productId).get(idz).get('Name Translation')[0].Translation__r.Count_of_Translation_Criteria__c){
                                system.debug('******Translation Criteria**NAME******'+ProductIdWithTranslationMap.get(productId).get(idz).get('Name Translation')[0]);
                                if(ProductIdWithTranslationMap.get(productId).get(idz).get('Name Translation')[0].Translation__r.French_Name__c!=null){
                                    oLIrec.French_Name__c = ProductIdWithTranslationMap.get(productId).get(idz).get('Name Translation')[0].Translation__r.French_Name__c;
                                }
                                if(ProductIdWithTranslationMap.get(productId).get(idz).get('Name Translation')[0].Translation__r.Rogers_Branded_Name__c!=null){
                                    oLIrec.Rogers_Branded_Name__c = ProductIdWithTranslationMap.get(productId).get(idz).get('Name Translation')[0].Translation__r.Rogers_Branded_Name__c;
                                }
                            }
                        }
                        if(ProductIdWithTranslationMap.get(productId).get(idz).containsKey('SOC Translation')){
                            for(Translation_Criteria__c tc: ProductIdWithTranslationMap.get(productId).get(idz).get('SOC Translation')){
                                
                                String sValue;
                                Schema.SObjectField field;
                                if(mapFieldAPIandValueLineItem.get(tc.Field_Name__c)!=null){
                                    field = mapFieldAPIandValueLineItem.get(tc.Field_Name__c);
                                    sValue = String.valueOf(oLIrec.get(field));
                                    system.debug('**field*****sValue****'+sValue+'*******'+field);
                                }//countOfFrenchTransCriteria
                                if(field != null){
                                     if(tc.Field_Value__c == sValue){
                                        countOfSOCTransCriteria++;
                                    }
                                }
                            }
                            system.debug('***countOfSOCTransCriteria********'+countOfSOCTransCriteria);
                            if(countOfSOCTransCriteria == ProductIdWithTranslationMap.get(productId).get(idz).get('SOC Translation')[0].Translation__r.Count_of_Translation_Criteria__c){
                                system.debug('******Translation Criteria**SOC******'+ProductIdWithTranslationMap.get(productId).get(idz).get('SOC Translation')[0]);
                                if(ProductIdWithTranslationMap.get(productId).get(idz).get('SOC Translation')[0].Translation__r.Original_SOC__c!=null){
                                    oLIrec.SOC_Code__c = ProductIdWithTranslationMap.get(productId).get(idz).get('SOC Translation')[0].Translation__r.Original_SOC__c;
                                }
                            }
                        }
                        
                    }
                }
                              
            }
             //If French Name is not populated through Translation object then populate it with 'Product name on Document'
            if(String.IsBlank(oLIrec.French_Name__c)){
                    oLIrec.French_Name__c = oLIrec.Product_Name_on_Document__c;
            }
             system.debug('******French_Name__c*********'+oLIrec.French_Name__c +'****Rogers_Branded_Name__c*****'+oLIrec.Rogers_Branded_Name__c+'*****oLIrec.SOC_Code__c**'+oLIrec.SOC_Code__c);
            setLIUpdate.add(oLIrec);
        }
        //Updating the LI for the French name
        if(setLIUpdate != null && setLIUpdate.size() > 0){
            LIST<Apttus_Config2__LineItem__c> lstLIUpdate = new LIST<Apttus_Config2__LineItem__c>(setLIUpdate); 
            update lstLIUpdate;
        }
    }

}